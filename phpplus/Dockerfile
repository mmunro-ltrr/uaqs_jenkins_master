# PHP5 augmented with Composer and Drush.
# Stolen from Rob Loach's official images for these: for the originals, see
# https://github.com/RobLoach/docker-composer
# and
# https://github.com/RobLoach/drush-docker
FROM php:5-cli

# Debian packages (PHP is non-standard, from source in the base image)
RUN apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get install -y \
    libfontconfig1-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng12-dev \
    libbz2-dev \
    libxslt-dev \
    libldap2-dev \
    mysql-client \
    php-pear \
    curl \
    git \
    subversion \
    sudo \
    unzip \
    wget \
    zip \
  && rm -r /var/lib/apt/lists/*

RUN echo 'America/Phoenix' > /etc/timezone && dpkg-reconfigure --frontend=noninteractive tzdata

# Extensions to the base PHP
RUN docker-php-ext-install bcmath mcrypt zip bz2 mbstring pcntl xsl \
  && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-install gd \
  && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
  && docker-php-ext-install ldap \
  && docker-php-ext-install pdo_mysql

# Memory Limit
RUN echo "memory_limit=-1" > $PHP_INI_DIR/conf.d/memory-limit.ini

# Time Zone
RUN echo "date.timezone=${PHP_TIMEZONE:-America/Phoenix}" > $PHP_INI_DIR/conf.d/date_timezone.ini

# Disable Populating Raw POST Data
# Not needed when moving to PHP 7.
# http://php.net/manual/en/ini.core.php#ini.always-populate-raw-post-data
RUN echo "always_populate_raw_post_data=-1" > $PHP_INI_DIR/conf.d/always_populate_raw_post_data.ini

# Register the COMPOSER_HOME environment variable
ENV COMPOSER_HOME /composer

# Add global binary directory to PATH and make sure to re-export it
ENV PATH /composer/vendor/bin:$PATH

# Allow Composer to be run as root
ENV COMPOSER_ALLOW_SUPERUSER 1

ENV COMPOSER_VERSION 1.1.3

ENV DRUSH_VERSION 8.1.3

# Install Composer and Drush (the `-fsSL` on the drush curl is needed to redirect to s3)
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
  && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
  && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
  && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION} \
  && rm -rf /tmp/composer-setup.php \
  && curl -fsSL -o /usr/local/bin/drush "https://github.com/drush-ops/drush/releases/download/$DRUSH_VERSION/drush.phar" \
  && chmod 755 /usr/local/bin/drush

RUN groupadd -g 1000 jenkins \
  && useradd -u 1000 -g 1000 -s /bin/bash jenkins \
  && adduser jenkins sudo \
  && echo 'Defaults !authenticate' >> /etc/sudoers


# Just a shell, not drush or composer
CMD ["/bin/bash"]
